import pymorphy2


class MorphAnalyzerExt(pymorphy2.MorphAnalyzer):
	def __init__(self, path=None, ext_path=None):
		super().__init__(path=path)
		if ext_path is None:
			self.extDict = None
		else:
			self.extDict = pymorphy2.MorphAnalyzer(path=ext_path)

	def parse_several_words(self, token, sort=True):
		res = self.parse_several_words_ext(token, sort=False)
		words = token.split(" ")
		for word in words:
			res.extend(self.parse(word))
		if sort:
			res.sort(key=lambda x: x.score, reverse=True)
		return res

	def parse_several_words_ext(self, token, sort=True):
		res = self.parse_single_word_ext(token, sort=False)
		if len(res) == 0:
			words = token.split(" ")
			for word in words:
				res.extend(self.parse_single_word_ext(word, sort=False))
		if sort:
			res.sort(key=lambda x: x.score, reverse=True)
		return res

	def parse_single_word(self, word, sort=True):
		res = self.parse_single_word_ext(word, sort=False)
		res.extend(self.parse(word))
		res.sort(key=lambda x: x.score, reverse=True)
		if sort:
			res.sort(key=lambda x: x.score, reverse=True)
		return res

	def parse_single_word_ext(self, word, sort=True):
		res = []
		if self.extDict is not None:
			tmp = self.extDict.parse(word)
			for el in tmp:
				if el.is_known:
					res.append(el)
		if sort:
			res.sort(key=lambda x: x.score, reverse=True)
		return res


def main():
	morph = MorphAnalyzerExt(ext_path="dict")
	# morph = MorphAnalyzerExt()
	ls = morph.parse_several_words('коту в мешке', sort=False)
	for el in ls:
		print(el)

if __name__ == '__main__':
	main()
